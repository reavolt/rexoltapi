var admin = require('firebase-admin');
var serviceAccount = require('./rexolt-3af8a-firebase-adminsdk-ht5s7-d2df17b5fe.json');
var restify = require('restify');
var uniqid = require('uniqid');
var server = restify.createServer();
var mcache = require('memory-cache')
var _ = require('lodash')
server.use(restify.plugins.bodyParser());
var cloudinary = require('cloudinary');
cloudinary.config({
  cloud_name: 'rexolt',
  api_key: '823423444661556',
  api_secret: 'vE1I7u15Kyy_lpmHWYUtCnDu6ps'
});

var moment = require('moment-timezone');
const snoowrap = require('snoowrap');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://rexolt-3af8a.firebaseio.com'
});
var db = admin.firestore();
var algoliasearch = require('algoliasearch')
var client = algoliasearch('MOUZ9ZZGWG', 'df09f2c6fd79928a91e9c0c06e20bf76');
var index = client.initIndex('Songs');
const r = new snoowrap({
  userAgent: 'rexolt.rexolt.com',
  clientId: 'iFJXsEdwsT6WrA',
  clientSecret: 'KHAP3dAXcrhW1uvwrqRyYZakgmQ',
  refreshToken: '37672336-OZb3MpdXZ2oa9fWXHJNeHiDIIug'
});
var app = require('express')();
var server2 = require('http').Server(app);
var io = require('socket.io')(server2);

server2.listen(3006);

function getStatus2(socket) {
  var docRef = db.collection('data').doc('status');
  docRef.get().then(function (doc) {
    if (doc.exists) {
      socket.emit('getStatus', {
        success: true,
        data: doc.data()
      })
      console.log('shouldve sent?')
    } else {
      // doc.data() will be undefined in this case
      socket.emit('getStatus', {
        success: false,
        error: 'No document'
      })
      console.log("No such document!");
    }
  }).catch(function (error) {
    socket.emit('getStatus', {
      success: false,
      error: error
    })
    console.log("Error getting document:", error);
  });
}

io.on('connection', function (socket) {
  // On connection, Get Status
  getStatus2(socket)
  // Get Song when navigated to (/item/id)
  socket.on('getSong', function (id) {
    console.log(id)
    index.getObject(id, function (err, content) {
      console.log(content.objectID + ": " + content.toString());
      if (err) {
        socket.emit('getSong', {
          success: false,
          error: err
        })
        return console.log(err)
      }
      socket.emit('getSong', {
        success: true,
        content
      })
    });
  });
  // Add song
  socket.on('addSong', function (req) {
    if (req.password == process.env.rexoltpass) {
      cloudinary.v2.uploader.upload(req.art, {
        width: 270,
        height: 200,
        format: 'png'
      }, function (error, result) {
        if (error) {
          socket.emit('addSong', {
            success: false,
            error: 'cloudinary error'
          })
        }
        var artist = req.artist
        var password = req.password
        image_url = result.secure_url
        console.log(result)
        console.log(error)
        var data = {
          title: req.title,
          art: image_url,
          music: req.music,
          thread: req.thread,
          quality: req.quality,
          producer: req.producer,
          tag: req.tag,
          uploaded: moment().tz("America/New_York").format("M-D-YY, hA").replace('/', '-') + " EST",
          date: Date.now(),
          rawdate: new Date()
        }
        console.log(data)
        //Add to firestore
        try {
          var setDoc = db.collection('data').doc(artist).collection('songs').doc(req.title)
          var setWithOptions = setDoc.set({
            data
          });
        } catch (err) {
          console.log(err)
          socket.emit('addSong', {
            success: false,
            error: 'firebase error'
          })
        }
        //Add to algolia
        index.addObject({
          artist: artist,
          title: req.title,
          art: image_url,
          music: req.music,
          thread: req.thread,
          quality: req.quality,
          producer: req.producer,
          tag: req.tag,
          uploaded: moment().tz("America/New_York").format("M-D-YY, hA").replace('/', '-') + " EST",
          date: Date.now(),
          rawdate: new Date()
        }, function (err, content) {
          if (err) {
            socket.emit('addSong', {
              success: false,
              error: 'algolia error'
            })
          } else {
            socket.emit('addSong', {
              success: true,
              data
            })
          }
        })
      })
    } else {
      console.log('worng password')
      socket.emit('addSong', {
        success: false,
        error: 'wrong password'
      })
    }
  })

  //Remove Song
  socket.on('removeSong', function (id, password) {
    if (password == process.env.rexoltpass) {
      console.log(id)
      index.deleteObject(id, function (err, content) {
        console.log(content.objectID + ": " + content.toString());
        if (err) {
          socket.emit('removeSong', {
            success: false,
            error: err,
            title: content.objectID
          })
          return console.log(err)
        }
        socket.emit('removeSong', {
          success: true,
          title: content.objectID
        })
      })
    } else {
      socket.emit('removeSong', {
        success: false,
        error: 'wrong password'
      })
    }
  });
  socket.on('getSongs', function () {
    var browser = index.browseAll();
    var hits = [];

    browser.on('result', function onResult(content) {
      console.log('Finished!');
      console.log('We got %d hits', content.hits.length);
      hits = hits.concat(content.hits)
      socket.emit('getSongs', {
        success: true,
        hits
      })
    })
  });
  socket.on('getFresh', function () {
    r.getSubreddit('hiphopheads').getTop({
      time: 'day'
    }).map(post => output = {
      score: post.score,
      title: post.title,
      permalink: post.permalink
    }).then(data => {
      var obj = _.orderBy(data, 'score', 'desc')
      console.log(obj)
      socket.emit('getFresh', {
        success: true,
        obj
      })
    })
  });
})

function updateStatus(req, res, next) {
  var data = {
    title: req.body.title,
    description: req.body.description,
    updated: moment().tz("America/New_York").format("M-D-YY, hA").replace('/', '-') + " EST",
    date: Date.now(),
    rawdate: new Date()
  }
  //if (password == process.env.rexoltpass) {
  var shoulddelete = req.body.shoulddelete
  console.log(shoulddelete)
  if (shoulddelete == 'yes') {
    console.log('should delete is true, attempting delete')
    db.collection('data').doc('status').delete().then(function () {
      res.send({
        success: true
      })
    }).catch(function (err) {
      console.log(err)
      res.send({
        success: false,
        error: 'firebase error'
      })
    })
  } else {
    try {
      console.log('set data??')
      var setDoc = db.collection('data').doc('status')
      var setWithOptions = setDoc.set({
        data
      });
      res.send({
        success: true
      })
    } catch (err) {
      console.log(err)
      res.send({
        success: false,
        error: 'firebase error'
      })
    }
  }
  next();
  //}
}

function getStatus(req, res, next) {
  var docRef = db.collection('data').doc('status');
  docRef.get().then(function (doc) {
    if (doc.exists) {
      res.send({
        success: true,
        data: doc.data()
      })
    } else {
      // doc.data() will be undefined in this case
      res.send({
        success: false,
        error: 'No such document'
      })
      console.log("No such document!");
    }
  }).catch(function (error) {
    res.send({
      success: false,
      error: error
    })
    console.log("Error getting document:", error);
  });
  next();
}

function addSong(req, res, next) {
  cloudinary.v2.uploader.upload(req.body.art, {
    width: 270,
    height: 200,
    format: 'png'
  }, function (error, result) {
    var artist = req.body.artist
    var password = req.body.password
    image_url = result.secure_url
    console.log(result)
    console.log(error)
    var data = {
      title: req.body.title,
      art: image_url,
      music: req.body.music,
      thread: req.body.thread,
      quality: req.body.quality,
      producer: req.body.producer,
      tag: req.body.tag,
      uploaded: moment().tz("America/New_York").format("M-D-YY, hA").replace('/', '-') + " EST",
      date: Date.now(),
      rawdate: new Date()
    }
    console.log(data)
    if (password == process.env.rexoltpass) {
      //Add to firestore
      try {
        var setDoc = db.collection('data').doc(artist).collection('songs').doc(req.body.title)
        var setWithOptions = setDoc.set({
          data
        });
        res.send({
          success: true
        })
      } catch (err) {
        console.log(err)
        res.send({
          success: false,
          error: 'firebase error'
        })
      }
      //Add to algolia
      index.addObject({
        artist: artist,
        title: req.body.title,
        art: image_url,
        music: req.body.music,
        thread: req.body.thread,
        quality: req.body.quality,
        producer: req.body.producer,
        tag: req.body.tag,
        uploaded: moment().tz("America/New_York").format("M-D-YY, hA").replace('/', '-') + " EST",
        date: Date.now(),
        rawdate: new Date()
      }, function (err, content) {
        if (err) {
          res.send({
            success: false,
            error: 'algolia error'
          })
        } else {
          res.send({
            success: true
          })
        }
      })
    } else {
      res.send({
        success: false,
        error: 'wrong password'
      })
    }
  })
  next();
}

function getDaily(req, res, next) {
  r.getSubreddit('hiphopheads').getTop({
    time: 'day'
  }).map(post => output = {
    score: post.score,
    title: post.title,
    permalink: post.permalink
  }).then(data => {
    var obj = _.orderBy(data, 'score', 'desc')
    console.log(obj)
    res.send({
      success: true,
      obj
    })
    next()
  })
}

function getSong(req, res, next) {
  var id = req.params.id
  console.log(id)
  index.getObject(id, function (err, content) {
    console.log(content.objectID + ": " + content.toString());
    if (err) {
      res.send({
        success: false,
        error: err
      })
      return console.log(err)
    }
    res.send({
      success: true,
      content
    })
  });
  next();
}

function reportSong(req, res, next) {
  var id = req.body.id
  var title = req.body.title
  var data = {
    id: req.body.id,
    title: req.body.title,
    reason: req.body.reason,
    date: moment().tz("America/New_York").format("M-D-YY, hA").replace('/', '-') + " EST"
  }
  console.log(id)
  try {
    console.log('set data??')
    var setDoc = db.collection('reports').doc(title + '-' + id)
    var setWithOptions = setDoc.set({
      data
    });
    res.send({
      success: true
    })
  } catch (err) {
    console.log(err)
    res.send({
      success: false,
      error: 'firebase error'
    })
  }
  next();
}

server.use(restify.plugins.queryParser({
  mapParams: true
}));
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.bodyParser({
  "params": true
}))
const corsMiddleware = require('restify-cors-middleware')

const cors = corsMiddleware({
  preflightMaxAge: 5, //Optional
  origins: ['http://localhost:8080', '*'],
  allowHeaders: ['*'],
  exposeHeaders: ['*']
})
var apicache = require('apicache')
var cache = apicache.middleware
apicache.options({
  debug: true
})


server.pre(cors.preflight)
server.use(cors.actual)
//cms
server.post('/api/updatestatus', updateStatus);
server.post('/api/addsong', addSong);
//get
server.get('/api/getstatus', cache('5 minutes'), getStatus);
server.get('/api/getdaily', cache('20 minutes'), getDaily);
server.get('/api/getsong/:id', getSong);

//post
server.post('/api/report', reportSong);

server.listen(3005, function () {
  console.log('%s listening at %s', server.name, server.url);
});