var admin = require('firebase-admin');
var serviceAccount = require('./rexolt-3af8a-firebase-adminsdk-ht5s7-d2df17b5fe.json');
var uniqid = require('uniqid');
var _ = require('lodash')
var cloudinary = require('cloudinary');
cloudinary.config({
  cloud_name: 'rexolt',
  api_key: '823423444661556',
  api_secret: 'vE1I7u15Kyy_lpmHWYUtCnDu6ps'
});

var moment = require('moment-timezone');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://rexolt-3af8a.firebaseio.com'
});
var db = admin.firestore();
var algoliasearch = require('algoliasearch')
var client = algoliasearch('MOUZ9ZZGWG', 'df09f2c6fd79928a91e9c0c06e20bf76');
var songs = client.initIndex('Songs');
var snippets = client.initIndex('Snippets');
var express = require('express')
var app = express()
app.listen(3002, () => console.log(`Example app listening on port 3002`))


// Get Song when navigated to (/item/id)
app.get('/getitem/:id', function (req, res) {
  var queries = [{
    indexName: 'Songs',
    query: req.params.id,
    params: {
      hitsPerPage: 1,
      facetFilters: 'objectID:' + req.params.id
    }
  }, {
    indexName: 'Snippets',
    query: req.params.id,
    params: {
      hitsPerPage: 1,
      facetFilters: 'objectID:' + req.params.id
    }
  }];
  client.search(queries, function searchCallback(err, content) {
    if (err) throw err;

    var mutatedResult
    if (content.results[0].nbHits <= 0) {
      mutatedResult = content.results[1].hits
    } else {
      mutatedResult = content.results[0].hits
    }
    res.json(mutatedResult);
  });
})
// Add song
/* socket.on('addSong', function (req) {
  if (req.password == process.env.rexoltpass) {
    cloudinary.v2.uploader.upload(req.art, {
      width: 270,
      height: 200,
      fetch_format: "auto",
      quality: auto
    }, function (error, result) {
      if (error) {
        socket.emit('addSong', {
          success: false,
          error: 'cloudinary error'
        })
      }
      var artist = req.artist
      var password = req.password
      image_url = result.secure_url
      console.log(result)
      console.log(error)
      var data = {
        title: req.title,
        art: image_url,
        music: req.music,
        thread: req.thread,
        quality: req.quality,
        producer: req.producer,
        tag: req.tag,
        uploaded: moment().tz("America/New_York").format("M-D-YY, hA").replace('/', '-') + " EST",
        date: Date.now(),
        rawdate: new Date()
      }
      console.log(data)
      //Add to firestore
      try {
        var setDoc = db.collection('data').doc(artist).collection('songs').doc(req.title)
        var setWithOptions = setDoc.set({
          data
        });
      } catch (err) {
        console.log(err)
        socket.emit('addSong', {
          success: false,
          error: 'firebase error'
        })
      }
      //Add to algolia
      index.addObject({
        artist: artist,
        title: req.title,
        art: image_url,
        music: req.music,
        thread: req.thread,
        quality: req.quality,
        producer: req.producer,
        tag: req.tag,
        uploaded: moment().tz("America/New_York").format("M-D-YY, hA").replace('/', '-') + " EST",
        date: Date.now(),
        rawdate: new Date()
      }, function (err, content) {
        if (err) {
          socket.emit('addSong', {
            success: false,
            error: 'algolia error'
          })
        } else {
          socket.emit('addSong', {
            success: true,
            data
          })
        }
      })
    })
  } else {
    console.log('worng password')
    socket.emit('addSong', {
      success: false,
      error: 'wrong password'
    })
  }
})

//Remove Song
socket.on('removeSong', function (id, password) {
  if (password == process.env.rexoltpass) {
    console.log(id)
    index.deleteObject(id, function (err, content) {
      console.log(content.objectID + ": " + content.toString());
      if (err) {
        socket.emit('removeSong', {
          success: false,
          error: err,
          title: content.objectID
        })
        return console.log(err)
      }
      socket.emit('removeSong', {
        success: true,
        title: content.objectID
      })
    })
  } else {
    socket.emit('removeSong', {
      success: false,
      error: 'wrong password'
    })
  }
});
// Get list of songs
socket.on('getSongs', function () {
  var browser = index.browseAll();
  var hits = [];

  browser.on('result', function onResult(content) {
    console.log('Finished!');
    console.log('We got %d hits', content.hits.length);
    hits = hits.concat(content.hits)
    socket.emit('getSongs', {
      success: true,
      hits
    })
  })
});
// Gets fresh tab data
socket.on('getFresh', function () {
  r.getSubreddit('hiphopheads').getTop({
    time: 'day'
  }).map(post => output = {
    score: post.score,
    title: post.title,
    permalink: post.permalink
  }).then(data => {
    var obj = _.orderBy(data, 'score', 'desc')
    console.log(obj)
    socket.emit('getFresh', {
      success: true,
      obj
    })
  })
});
// Get and Post report song
socket.on('reportSong', function (id, title, reason) {
  var data = {
    id: id,
    title: title,
    reason: reason,
    date: moment().tz("America/New_York").format("M-D-YY, hA").replace('/', '-') + " EST"
  }
  console.log('report song data: ' + id)
  try {
    console.log('set data??')
    var setDoc = db.collection('reports').doc(title + '-' + id)
    var setWithOptions = setDoc.set({
      data
    });
    socket.emit('reportSong', {
      success: true,
    })
  } catch (err) {
    console.log(err)
    socket.emit('reportSong', {
      success: false
    })
  }
})
socket.on('editSong', function (data) {
  console.log('data: ' + JSON.stringify(data))
  cloudinary.v2.uploader.upload(data.art, {
    width: 270,
    height: 200,
    format: 'png'
  }, function (error, result) {
    if (error) {
      socket.emit('editSong', {
        success: false,
        error: 'cloudinary error'
      })
    }
    if (data.password) {
      index.partialUpdateObject({
        artist: data.artist,
        title: data.title,
        art: result.secure_url,
        music: data.music,
        thread: data.thread,
        quality: data.quality,
        producer: data.producer,
        tag: data.tag,
        ...(data.released ? {
          released: data.released
        } : ''),
        objectID: data.objectID
      }, function (err, content) {
        if (err) {
          socket.emit('editSong', {
            success: false,
            error: err
          })
        } else {
          socket.emit('editSong', {
            success: true
          })
        }
        console.log(content);
      })
    } else {
      socket.emit('editSong', {
        success: false,
        error: 'Wrong Password'
      })
    }
  });
})
*/